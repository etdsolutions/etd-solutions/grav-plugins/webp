<?php
/**
 * @author ETD Solutions
 * @copyright Copyright (c) 2021 ETD Solutions
 * @package Grav\Plugin
 */

namespace Grav\Plugin\ETDWebp\Helper;

use SplFileInfo;

class View
{
    /** @var Image */
    private $image;

    /**
     * View constructor.
     */
    public function __construct()
    {
        $this->image = new Image();
    }

    /**
     * @param string $imagePath
     * @return string
     */
    public function changeImagePath(string $imagePath): string
    {
        $image = new SplFileInfo($imagePath);

        $pathname = $image->getPathInfo()->getPathname();
        $imageName = substr(strrchr($imagePath, DIRECTORY_SEPARATOR), 1);
        $imageNameWithoutExtension = substr($imageName, 0, strrpos($imageName, '.'));

        $webpPath = $this->image->getWebpPath($pathname, $imageNameWithoutExtension);

        //todo : remove first dot from webppath
        return file_exists($webpPath) ? ltrim($webpPath,'.') : $imagePath;
    }

    /**
     * @param string $imagePath
     * @return string
     */
    public function getImageExtension(string $imagePath): string
    {
        $image = new SplFileInfo($imagePath);

        return $image->getExtension();
    }
}
