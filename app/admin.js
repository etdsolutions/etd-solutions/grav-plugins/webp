/**
 * @author ETD Solutions
 * @copyright Copyright (c) 2021 ETD Solutions
 */

import progressbar from './modules/progressbar';

document.addEventListener("DOMContentLoaded", event => {
    progressbar();
});
